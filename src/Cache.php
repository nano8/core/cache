<?php

namespace laylatichy\nano\core;

use JetBrains\PhpStorm\Pure;
use laylatichy\nano\core\enums\CacheStatus;

final class Cache {
    private const MINS = 60;

    public function __construct(
        private CacheStatus $status = CacheStatus::ENABLED,
        private int $duration = 60 * self::MINS,
    ) {
    }

    #[Pure]
    public function getDuration(): int {
        return $this->duration / self::MINS;
    }

    public function setDuration(int $minutes): void {
        $this->duration = $minutes * self::MINS;
    }

    #[Pure]
    public function getStatus(): CacheStatus {
        return $this->status;
    }

    public function setStatus(CacheStatus $status): void {
        $this->status = $status;
    }

    public function disable(): void {
        $this->status = CacheStatus::DISABLED;
    }

    public function enable(): void {
        $this->status = CacheStatus::ENABLED;
    }

    #[Pure]
    public function isEnabled(): bool {
        return $this->status->getStatus();
    }
}
