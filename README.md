### nano/core/cache

[![release](https://img.shields.io/packagist/v/laylatichy/nano-core-cache?style=flat-square)](https://packagist.org/packages/laylatichy/nano-core-cache)
[![PHPStan](https://img.shields.io/badge/PHPStan-Level%20max-brightgreen.svg?style=flat&logo=php)](https://shields.io/#/)
[![Total Downloads](https://img.shields.io/packagist/dt/laylatichy/nano-core-cache?style=flat-square)](https://packagist.org/packages/laylatichy/nano-core-cache)

##### CI STATUS

-   dev [![pipeline status](https://gitlab.com/nano8/core/cache/badges/dev/pipeline.svg)](https://gitlab.com/nano8/core/cache/-/commits/dev)
-   master [![pipeline status](https://gitlab.com/nano8/core/cache/badges/master/pipeline.svg)](https://gitlab.com/nano8/core/cache/-/commits/master)
-   release [![pipeline status](https://gitlab.com/nano8/core/cache/badges/release/pipeline.svg)](https://gitlab.com/nano8/core/cache/-/commits/release)

#### Install

-   `composer require laylatichy/nano-core-cache`
